# Install script for directory: /home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE FILE FILES
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Cholesky"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/CholmodSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Core"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Dense"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Eigen"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Eigenvalues"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Geometry"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Householder"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/IterativeLinearSolvers"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Jacobi"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/LU"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/MetisSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/OrderingMethods"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/PaStiXSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/PardisoSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/QR"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/QtAlignedMalloc"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SPQRSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SVD"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/Sparse"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SparseCholesky"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SparseCore"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SparseLU"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SparseQR"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/StdDeque"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/StdList"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/StdVector"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/SuperLUSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/UmfPackSupport"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/Eigen" TYPE DIRECTORY FILES "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

