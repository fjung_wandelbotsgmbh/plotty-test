# Install script for directory: /home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/install")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/unsupported/Eigen" TYPE FILE FILES
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/AdolcForward"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/AlignedVector3"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/ArpackSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/AutoDiff"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/BVH"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/EulerAngles"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/FFT"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/IterativeSolvers"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/KroneckerProduct"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/LevenbergMarquardt"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/MatrixFunctions"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/MoreVectorization"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/MPRealSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/NonLinearOptimization"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/NumericalDiff"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/OpenGLSupport"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/Polynomials"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/Skyline"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/SparseExtra"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/SpecialFunctions"
    "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/Splines"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xDevelx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/eigen3/unsupported/Eigen" TYPE DIRECTORY FILES "/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/source/unsupported/Eigen/src" FILES_MATCHING REGEX "/[^/]*\\.h$")
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/fjung/_main/dev/build/robot-workspace-generator/3rdparty/eigen/bin/unsupported/Eigen/CXX11/cmake_install.cmake")

endif()

